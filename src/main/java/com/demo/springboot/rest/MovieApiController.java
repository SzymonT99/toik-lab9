package com.demo.springboot.rest;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class MovieApiController {

    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    private MovieService movieService;

    @PostMapping("/movies")
    public ResponseEntity<Void> createMovie(@RequestBody CreateMovieDto createMovieDto){

        LOG.info("--- title: {}", createMovieDto.getTitle());
        LOG.info("--- year: {}", createMovieDto.getYear());
        LOG.info("--- image: {}", createMovieDto.getImage());

        Boolean addedStatus = movieService.addMovie(createMovieDto);

        return addedStatus != false
                ? new ResponseEntity<>(HttpStatus.CREATED)
                : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {

        LOG.info("--- get all movies: {}", movieService.getSortedDescendingList());

        return new ResponseEntity<>(movieService.getSortedDescendingList(), HttpStatus.OK);
    }


    @PutMapping("movies/{id}")
    public ResponseEntity<Void> updateMovieById(@PathVariable("id") Integer id, @RequestBody CreateMovieDto createMovieDto) {

        LOG.info("--- id changed CreateMovieDto: {}", id);
        LOG.info("--- changed title: {}", createMovieDto.getTitle());
        LOG.info("--- changed year: {}", createMovieDto.getYear());
        LOG.info("--- changed image: {}", createMovieDto.getImage());

        Boolean updatedStatus = movieService.updateMovie(id, createMovieDto);

        return updatedStatus != false
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    @DeleteMapping("movies/{id}")
    public ResponseEntity<Void> deletewMovieById(@PathVariable("id") Integer id){

        Boolean deletedStatus = movieService.deleteMovie(id);

        return deletedStatus != false
                ? new ResponseEntity<>(HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


}
