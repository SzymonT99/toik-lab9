package com.demo.springboot.service.impl;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.service.MovieService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieListDto movies;

    public MovieServiceImpl() {
        List<MovieDto> moviesList = new ArrayList<>();
        moviesList.add(new MovieDto(1,
                        "Piraci z Krzemowej Doliny",
                        1999,
                        "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
                );
        this.movies = new MovieListDto(moviesList);
    }

    @Override
    public MovieListDto getMovieList() {
        return this.movies;
    }

    @Override
    public
    Boolean addMovie(CreateMovieDto createMovieDto) {
        if (createMovieDto.getTitle() == null || createMovieDto.getYear() == null || createMovieDto.getImage() == null){
            return false;
        }
        else {
            Integer nextMovieId = this.movies.getMovies().size() + 1;
            MovieDto movieDto = new MovieDto(
                    nextMovieId,
                    createMovieDto.getTitle(),
                    createMovieDto.getYear(),
                    createMovieDto.getImage()
            );
            this.movies.getMovies().add(movieDto);

            return true;
        }
    }

    @Override
    public MovieListDto getSortedDescendingList() {

        List<MovieDto> sortedDescendingList =  movies.getMovies().stream()
                .sorted(Comparator.comparing(MovieDto::getMovieId).reversed())
                .collect(Collectors.toList());

        return new MovieListDto(sortedDescendingList);
    }

    @Override
    public Boolean updateMovie(Integer id, CreateMovieDto createMovieDto) {

        Integer positionMovieInList = null;
        for (int i = 0; i < this.movies.getMovies().size(); i++){
            if (this.movies.getMovies().get(i).getMovieId() == id){
                positionMovieInList = i;
            }
        }

        if (positionMovieInList != null){

            if (createMovieDto.getTitle() != null) {
                this.movies.getMovies().get(positionMovieInList).setTitle(createMovieDto.getTitle());
            }
            if (createMovieDto.getYear() != null) {
                this.movies.getMovies().get(positionMovieInList).setYear(createMovieDto.getYear());
            }
            if (createMovieDto.getImage() != null) {
                this.movies.getMovies().get(positionMovieInList).setImage(createMovieDto.getImage());
            }
            return true;
        }
        else {
            return false;
        }

    }

    @Override
    public Boolean deleteMovie(Integer id) {
        MovieDto movieDto = null;
        for (MovieDto currentMovie : this.movies.getMovies()){
            if (currentMovie.getMovieId() == id){
                movieDto = currentMovie;
            }
        }

        if (movieDto != null){

            this.movies.getMovies().remove(movieDto);

            return true;
        }
        else {
            return false;
        }
    }
}
