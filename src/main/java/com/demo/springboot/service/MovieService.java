package com.demo.springboot.service;

import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieListDto;

public interface MovieService {

    MovieListDto getMovieList();
    Boolean addMovie(CreateMovieDto createMovieDto);
    MovieListDto getSortedDescendingList();
    Boolean updateMovie(Integer id, CreateMovieDto createMovieDto);
    Boolean deleteMovie(Integer id);
}
